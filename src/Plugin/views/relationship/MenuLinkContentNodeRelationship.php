<?php

namespace Drupal\menu_link_content_view_relationship\Plugin\views\relationship;

use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;

/**
 * Adds a relationship between the menu_link_content and node database.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("menu_link_content_node_relationship")
 */
class MenuLinkContentNodeRelationship extends RelationshipPluginBase {
}
